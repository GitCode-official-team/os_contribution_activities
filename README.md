# **仓颉编程语言体验有奖征文**

![开始下载.jpg](https://raw.gitcode.com/Gitcode-offical-team/os_contribution_activities/attachment/uploads/0ecec170-bb7b-432c-bf0f-4fec7975fb0d/开始下载.jpg '开始下载.jpg')

## **活动介绍**
[仓颉编程语言官网](https://cangjie-lang.cn/?utm_source=os_contribution_activities)已上线，提供版本下载、在线运行、文档体验等功能。其中，下载模块提供了仓颉编程语言的软件包、 IDE 的下载以及安装指南，在线体验功能则让开发者能够快速体验仓颉语言，开发者可以通过在线体验提供的预置代码，简单了解仓颉语言的程序结构，面向对象的开发范式、函数开发范式、多线程编程以及网络编程，快速了解仓颉编程的概貌。

为鼓励更多开发者探索仓颉编程语言，现诚邀各位开发者通过官网[在线体验](https://cangjie-lang.cn/playground?utm_source=os_contribution_activities)/[下载使用](https://cangjie-lang.cn/download/0.53.13?utm_source=os_contribution_activities)，参与仓颉体验有奖征文活动。

## **参与方式**
- 活动时间：2024年11月20日 - 2024年12月31日
- 围绕仓颉编程语言使用体验、功能剖析、实战案例、改进建议等参考方向，原创写作不少于500字内容
- 在活动期间（2024年11月20日 - 2024年12月31日）将内容发布在CSDN平台，添加小助手GitCode代码君微信（微信号：GitCodeassistant）发送投稿链接
- 每位开发者投稿数量不限，可针对不同细分主题进行创作
- 评优标准：内容的逻辑清晰、图文并茂、技术理解透彻程度，文章数据表现

## **活动奖品**
获评为优质内容的文章作者可获得100 面值京东卡激励，奖励文章数量上限50篇，一位作者可获得多张京东卡激励，活动结束后发放。

*说明：奖品由我方代扣代缴个税 ，个税由获奖者承担

